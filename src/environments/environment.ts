// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyBOoJNcE6o8QC2t-w1dp_yRSkerD_C3I5s",
    authDomain: "test2021-a.firebaseapp.com",
    projectId: "test2021-a",
    storageBucket: "test2021-a.appspot.com",
    messagingSenderId: "484246591145",
    appId: "1:484246591145:web:f755108dba008979ff7bb8"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
